<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?144"></script>
    <link href="<?php echo base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo base_url()?>js/jquery.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <link href="<?php echo base_url()?>css/blog-post.css" rel="stylesheet">
</head>
<body>
  <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
  <!-- Overlay -->
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#bs-carousel" data-slide-to="1"></li>
    <li data-target="#bs-carousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <?php $query = $this->db->query('SELECT * FROM news ORDER BY id LIMIT 3');
    $qwer = 0;
        foreach ($query->result() as $row)
        {   ?>
              <div class="item slides <?php if($qwer==0) echo "active"; $qwer=1?>">
                <div class="overlay"></div>
                <div class="slide-1">
                  <img src="<?php echo base_url()?>images/<?php echo $row->image ?>">
                </div>
                <div class="hero">
                  <hgroup>
                      <h1><?php echo $row->title ?></h1>
                  </hgroup>
                  <a class="btn btn-hero btn-lg" role="button" href="<?php echo base_url()?>index.php/blog/index/<?php echo $row->link ?>">Читать</a>
                </div>
              </div>

              <?php } ?>
  </div>
</div>
<div class="see-what-else" style="float:center;">К другим новостям...</div>
</div>
<div class="container marketing">
  <div class="row">
    <?php $query = $this->db->query('SELECT * FROM news ORDER BY id DESC');
        foreach ($query->result() as $row)
        {   ?>
              <div class="block col-lg-4">
                <a href="<?php echo base_url()?>index.php/blog/index/<?php echo $row->link ?>">
                  <img  src="<?php echo base_url()?>images/<?php echo $row->image ?>" alt="" width="350" height="220">
                  <h2><?php echo $row->title ?></h2>
                </a>
                  <p><?php echo $row->desc ?></p>

              </div>
        <?php } ?>
  </div>
</div>
</body>
</html>
