<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Добавить новость</title>
    <link href="<?php echo base_url()?>css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="col-sm-4"></div>
    <div class="container col-sm-4"  style="margin-top:150px;">
    <form action="add" method="post">
      <div class="form-group row">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Title</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" name="title">
        </div>
      </div>
      <div class="form-group row">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Image</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="smFormGroupInput"  name="image">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-10">
          <a href="<?php echo base_url()?>index.php/welcome/upload_photo">Загрузить фотографию</a>
        </div>
      </div>
      <div class="form-group row">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Link</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="smFormGroupInput"  name="link">
        </div>
      </div>
      <div class="form-group row">
        <label for="exampleTextarea">Description</label>
        <textarea class="form-control" id="exampleTextarea" name="desc" rows="5" cols="40" name="text"></textarea>
      </div>
      <div class="form-group row">
        <label for="exampleTextarea">Text</label>
        <textarea class="form-control" id="exampleTextarea" name="text" rows="40" cols="70" name="text"></textarea>
      </div>
      <button type="submit" name="add" class="btn btn-primary">Submit</button>
    </form>
  </div>
    <?php
    if(isset($_POST['add']))
    {
      $title = strip_tags(trim($_POST['title']));
      $text = strip_tags(trim($_POST['text']));
      $image = strip_tags(trim($_POST['image']));
      $link = strip_tags(trim($_POST['link']));
      $desc = strip_tags(trim($_POST['desc']));
      $data = array(
        'title' => $title,
        'text' => $text,
        'desc' => $desc,
        'image' => $image,
        'link' => $link,
      );
      $this->db->insert('news', $data);
      echo "Новость успешно добавлена!";
    }
     ?>
  </body>
</html>
