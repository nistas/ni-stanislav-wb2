
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, contact, form, icons" />

	<title>Footer Distributed With Contact Form</title>

	<link rel="stylesheet" href="<?php echo base_url()?>css/demo.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/footer-distributed-with-contact-form.css">

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">

</head>
	<body>
		<footer class="footer-distributed">

			<div class="footer" style="text-align:center;">

				<img src="<?php echo base_url()?>images/logo.png" alt="">

				<p class="footer-company-name">Ni Stanislav &copy; 2017</p>

        <script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>

<!-- VK Widget -->
<script type="text/javascript">
VK.Widgets.Group("vk_groups", {mode: 3, color1: '292C2F', color2: 'FFFFFF', color3: '565151'}, 54554368);
</script>
			</div>

		</footer>

	</body>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-97256352-1', 'auto');
	  ga('send', 'pageview');

	</script>

</html>
