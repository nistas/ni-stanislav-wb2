<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="<?php echo base_url()?>images/1.ico">
    <meta charset="utf-8">
    <link href="<?php echo base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>css/navbar.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url()?>speller/spell.js"></script>
    <meta name="description" content="">
    <title>Всё о Казахстанской музыке и музыкантах. Новости Казахской эстрады, связь между музыкантами!</title>
    <meta name="keywords" content="пройдет,Алматы,концерт,Тува,фестиваль,джаз,рок,дворец республики,балуан шолак,музыка,музыканты,объявление музыкантам,поиск музыкантов">
    <meta name="description" content="Всё о Казахстанской музыке и о музыкантах.Новости Казахской эстрады, связь между музыкантами. Возможность общения друг с другом.Действующий блог о актуальных новостях мира музыки">
</head>
<body>
    <h1 hidden="true">ProfMusic Kz</h1>
    <a class="navbar-brand" href="<?php echo base_url();?>"><img class="logo" src="<?php echo base_url()?>images/logo.png" height="110" width="110" alt=""></a>
</body>
</html>
