<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$db_obj =  $this->load->database('default');
		$this->load->view('templates/header');
		$this->load->view('index');
		$this->load->view('templates/footer');
	}

	function login()
	{
		  $this->load->database();
			$this->load->view('templates/header');
			$this->load->view('login');
			$this->load->view('templates/footer');
	}

	function upload_photo()
	{
		if(isset($_POST['upload']))
		{
			$config['upload_path']='./images/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '1000';
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			echo "Файл успешно загружен";
		}
		else {
			$this->load->view('upload_view');
		}
	}

}
