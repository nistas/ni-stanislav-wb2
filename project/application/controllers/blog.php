<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class blog extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function index($link)
	{
    $data = array(
      'link' => $link
    );
		$db_obj =  $this->load->database('default');
		$this->load->view('templates/header');
		$this->load->view('blog',$data);
    $this->load->view('templates/footer');
	}
}
